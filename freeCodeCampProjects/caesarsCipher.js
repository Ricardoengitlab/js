function rot13(str) {
  var dict = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];

  var newStr = Array.from(str);

  for (let index = 0; index < newStr.length; index++) {
    var re = /[^A-Za-z]/g;
    if (!re.test(newStr[index])) {
      var newIndex = (dict.indexOf(newStr[index]) + 13) % 26;
      newStr[index] = dict[newIndex];
    }
  }

  return newStr.join("");
}

rot13("SERR PBQR PNZC");
