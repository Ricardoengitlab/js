function telephoneCheck(str) {
  var variables = [
    "NNN-NNN-NNNN",
    "(NNN)NNN-NNNN",
    "(NNN) NNN-NNNN",
    "NNN NNN NNNN",
    "NNNNNNNNNN",
    "1 NNN NNN NNNN",
  ];
  var lista = [];

  for (var i = 0; i < str.length; i++) {
    if (
      str[i] == "(" ||
      str[i] == ")" ||
      str[i] == "-" ||
      str[i] == " " ||
      str[i] == "1"
    ) {
      lista.push(str[i]);
    } else {
      lista.push("N");
    }
  }

  if (lista[0] == 1) {
    lista = lista.slice(1, lista.length);
    if (lista[0] == " ") {
      lista = lista.slice(1, lista.length);
    }
  }

  for (let index = 0; index < variables.length; index++) {
    if (variables[index] === lista.join("")) {
      return true;
    }
  }

  return false;
}

console.log(telephoneCheck("1 (555) 555-5555"));
// telephoneCheck("(555)555-5555");
// telephoneCheck("(555) 555-5555");
// telephoneCheck("5555555555");
// telephoneCheck("1(555)555-5555");
// telephoneCheck("123**&!!asdf#");
// telephoneCheck("(6054756961)");
