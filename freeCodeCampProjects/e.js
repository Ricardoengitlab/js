function reverse(str) {
    return str.split("").reverse().join("");
}

function checkCashRegister(price, cash, cid) {
    var coins = [1, 5, 10, 25, 100, 500, 1000, 2000, 10000];
    var exit = {
            "status": "",
            "change": []
        }
        // Here is your change, ma'am.

    var lista = [];
    var lista2 = [];
    for (var i = 0; i < cid.length; i++) {
        lista.push(Math.round(cid[i][1] * 100))
    }


    lista = lista.reverse();
    coins = coins.reverse();
    var num = cash - price;
    num *= 100;
    num = Math.round(num);


    var i = 0;
    while (num > 0) {
        if (num % coins[i] == 0) {
            if (lista[i] >= coins[i]) {
                lista2.push(coins[i]);
                num -= coins[i];
                lista[i] -= coins[i];
            } else {
                //lista.push(0);
                i++;
            }
        } else {

            if (num > coins[i]) {
                if (lista[i] >= coins[i]) {
                    lista2.push(coins[i]);
                    num -= coins[i];
                    lista[i] -= coins[i];
                } else {
                    i++;
                }
            } else {
                //lista2.push(0)
                i++;
            }
        }
        if (i == 9) {
            break;
        }
    }


    //console.log(lista2);
    var listatmp = [];
    var sum = 0;
    for (var i = 0; i < lista2.length + 1; i++) {
        if (i == lista2.length + 1) {
            break;
        } else {
            if (lista2[i] == 0) {
                listatmp.push(0)
            } else {
                if (lista2[i] == lista2[i + 1]) {
                    sum += lista2[i];
                } else {
                    sum += lista2[i];
                    listatmp.push(sum);
                    sum = 0;
                }
            }
        }
    }
    //console.log(lista2);
    //console.log(listatmp);

    var tmp = 0;
    for (var i = 0; i < lista2.length; i++) {
        tmp += lista2[i];
    }

    var tmp2 = 0;
    for (var i = 0; i < cid.length; i++) {
        tmp2 += cid[i][1];
    }



    tmp /= 100;
    console.log(tmp);
    console.log(tmp2);
    if (tmp == tmp2 && tmp == (cash - price)) {
        exit.status = "CLOSED";
        exit.change = cid;
        console.log(exit);
        return exit;
    }
    if (tmp == (cash - price)) {
        exit.status = "OPEN";
        for (var i = 0; i < 9; i++) {
            if (listatmp[i] == undefined) {
                listatmp.push(0);
            }
        }

        var y = [
            ["PENNY", .01],
            ["NICKEL", .05],
            ["DIME", .1],
            ["QUARTER", .25],
            ["ONE", 1],
            ["FIVE", 5],
            ["TEN", 10],
            ["TWENTY", 20],
            ["ONE HUNDRED", 100]
        ];

        var textt = [];


        let unique = [...new Set(lista2)];
        //console.log(unique);

        for (var i = 0; i < unique.length; i++) {

            for (var j = 0; j < 9; j++) {
                if (unique[i] / 100 == y[j][1]) {
                    textt.push(y[j][0])
                }
            }
        }
        //console.log(listatmp);

        y = y.reverse();

        var final = []
        for (var i = 0; i < textt.length; i++) {
            if (listatmp[i] == 0) {
                continue;
            } else {
                final.push([textt[i], listatmp[i] / 100]);
            }
        }

        exit.change = final;
        return exit;
    } else {
        if (tmp < (cash - price)) {
            exit.status = "INSUFFICIENT_FUNDS";
            exit.change = [];
        } else {

        }
    }



    console.log(exit);
    return exit;
}


checkCashRegister(19.5, 20, [
    ["PENNY", 0.5],
    ["NICKEL", 0],
    ["DIME", 0],
    ["QUARTER", 0],
    ["ONE", 0],
    ["FIVE", 0],
    ["TEN", 0],
    ["TWENTY", 0],
    ["ONE HUNDRED", 0]
]);