import React, { useState } from "react";
import "./App.css";

var buttons = [
  "add",
  "multiply",
  "seven",
  "eight",
  "nine",
  "subtract",
  "four",
  "five",
  "six",
  "divide",
  "one",
  "two",
  "three",
  "zero",
  "decimal",
];
var simbols = [
  "+",
  "*",
  "7",
  "8",
  "9",
  "-",
  "4",
  "5",
  "6",
  "/",
  "1",
  "2",
  "3",
  "0",
  ".",
];

function App(params) {
  const [display, setDisplay] = useState("0");

  function Buttons() {
    return (
      <div>
        {buttons.map((val, index) => (
          <button
            key={index}
            id={val}
            value={simbols[index]}
            onClick={(e) => setDisplay(display === "0" ? e.target.value : display + e.target.value)}
          >
            {simbols[index]}
          </button>
        ))}
      </div>
    );
  }

  function Calc() {
    try {
      var result = eval(display);
      console.log(result);
      setDisplay(result);
    } catch (error) {}
  }

  return (
    <div id="calculator">
      <div id="display">{display}</div>
      <button onClick={(e) => setDisplay("0")} id="clear">AC</button>
      <Buttons />
      <button onClick={(e) => Calc()} id="equals">=</button>
    </div>
  );
}

export default App;
