import React from 'react';
import logo from './logo.svg';
import './App.css';



class Break extends React.Component{
  render(){
    return(
      <div id="break">
        <h1 id="break-label">Break</h1>
        <button id="break-decrement" onClick={e => this.props.handleClick(-1, "break")}>Decrement</button>
        <h3 id="break-length">{this.props.break}</h3>
        <button id="break-increment" onClick={e => this.props.handleClick(1, "break")}>Increment</button>
      </div>
    )
  }
}

class Session extends React.Component{
  render(){
    return(
      <div id="session">
        <h1 id="session-label">Session</h1>
        <button id="session-decrement" onClick={e => this.props.handleClick(-1, "session")}>Decrement</button>
        <h3 id="session-length">{this.props.session}</h3>
        <button id="session-increment" onClick={e => this.props.handleClick(1, "session")}>Increment</button>
      </div>
    )
  }
}


class App extends React.Component {


  constructor(props){
    super(props);
    this.state = {
      break: 5,
      session: 25,
      minutes: 25,
      seconds: 0,
      secondsAux: 60,
      running: false,
      s: true
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleStop = this.handleStop.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleClick(e, type){
    if(type === "break"){
      this.setState({
        break: this.state.break + e > 60  || this.state.break + e < 1 ? this.state.break : this.state.break + e
      })
    }else{
      this.setState({
        session: this.state.session + e > 60  || this.state.session + e  < 1 ? this.state.session : this.state.session + e,
        minutes: this.state.session + e > 60  || this.state.session + e < 1 ? this.state.session : this.state.session + e,
      })
    }
  }

  handleStop(){
    if(this.state.running === true){
      clearInterval(this.timerId);
      this.setState({
        running: false        
      })
    }else{
      this.setState({
        running: true,
      })
      this.timerId = setInterval(
        () => this.tick(), 1000
      );    }
  }


  tick(){
    if(this.state.secondsAux - 1 === 59){

      if(this.state.minutes === 0){

        if(this.state.s === true){
          this.audioBeep.play();
          this.setState({
            minutes: this.state.break,
            seconds: 60,
            s: false
          })
        }else{
          this.audioBeep.play();
          this.setState({
            minutes: this.state.session,
            seconds: 60,
            s: true
          })
        }
      }else{
        this.setState({
          minutes: this.state.minutes - 1,
          seconds: (this.state.secondsAux - 1) % 60,
          secondsAux: this.state.secondsAux -1
        })
      }
    }else{
        this.setState({
          seconds: (this.state.secondsAux - 1) === 0? 0 : this.state.secondsAux - 1,
          secondsAux: this.state.secondsAux -1 === 0? 60 : this.state.secondsAux -1
        })
    }
}
  

  handleReset(){
    clearInterval(this.timerId);
    this.setState({
      break: 5,
      session: 25,
      minutes: 25,
      seconds: 0,
      secondsAux: 60,
      running: false,
      s: true
    })
    this.audioBeep.pause();
    this.audioBeep.currentTime = 0;
  }

  render(){
    return (
      <div className="App"> 
        <Break handleClick={this.handleClick} break={this.state.break}/>
        <Session handleClick={this.handleClick} session={this.state.session}/>
        <div id="time">
          <h1 id="timer-label">{this.state.s === true ? "Session" : "Break"}</h1>
          <h1 id="time-left">{this.state.minutes.toString().length === 1 ? "0" + this.state.minutes.toString() : this.state.minutes}:{this.state.seconds.toString().length === 1 ? "0" + this.state.seconds.toString() : this.state.seconds }</h1>
          <button id="start_stop" onClick={this.handleStop}>Start-Stop</button>
          <button id="reset" onClick={this.handleReset}>Reset</button>
          <audio id="beep" preload="auto" 
          src="https://goo.gl/65cBl1"
          ref={(audio) => { this.audioBeep = audio; }} />
        </div>
      </div>
    );
  }

}

export default App;