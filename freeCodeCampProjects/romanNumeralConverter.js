function convertToRoman(num) {
    var mods = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    var simbolos = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]

    var str = "";
    var i = 0;
    while (num > 0) {
        if (num % mods[i] == 0) {
            str = str.concat(simbolos[i]);
            num -= mods[i];
        } else {
            if (num > mods[i]) {
                str = str.concat(simbolos[i]);
                num -= mods[i];
            } else { i++ }
        }
    }
    return str;
}

convertToRoman(1001);
convertToRoman(11);
convertToRoman(42);
convertToRoman(56);
convertToRoman(36);
convertToRoman(7);