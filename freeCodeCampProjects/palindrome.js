function palindrome(str) {
  str = str.replace(/[^A-Za-z0-9]/g, '');
  str = Array.from(str.replace(" ", "").toLowerCase());
  var newStr = Array.from(str).reverse();

  try {
    for (let index = 0; index < str.length; index++) {
      if (str[index] != newStr[index]) {
        return false;
      }
    }
  } catch (error) {
    return false;
  }

  return true;
}

palindrome("eye")
