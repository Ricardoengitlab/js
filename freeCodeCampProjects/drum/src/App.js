import React from 'react';
import logo from './logo.svg';
import './App.css';

var bankOne = [
 'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3',
 'https://s3.amazonaws.com/freecodecamp/drums/Brk_Snr.mp3'
];

var letters = ["Q", "W", "E", "A", "S", "D", "Z", "X", "C"]

class Audio extends React.Component{

  constructor(props){
    super(props);
    this.state = {

    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e){
    const doc = document.getElementById(e)
    
    doc.play();
    doc.currentTime = 0;
  }

  render(){
    return(
      <div id="pad">
        {letters.map((value, index) => 
          <button key={value.id} className="drum-pad" id={index} onClick={(e) => {this.handleClick(value); this.props.handleName(value);}} onKeyPress={(e) => {this.handleClick(value); this.props.handleName(value);}}>
            {value}
            <audio className="clip" id={value} preload="auto" 
            src={bankOne[index]}
            ref={(audio) => { this.value = audio; }} />
          </button>
        )}
      </div>
    )
  }
}

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      name: ""
    }
    this.handleName = this.handleName.bind(this)
  }

  handleName(e){
    this.setState({
      name: e
    })
  }

  render(){
    return (
    <div className="App">
      <div id="drum-machine">
        <div id="display">
          <h1>{this.state.name}</h1>
        </div>
        <Audio handleName = {this.handleName}/>
      </div>
    </div>
  );
 }
}

export default App;
